import { createRouter, createWebHistory } from 'vue-router';

const routes = [
  {
    path: '/',
    component: () => import('@/layouts/default/Default.vue'),
    children: [
      {
        path: '',
        name: 'home-view',
        component: () => import('@/views/Dashboard.vue'),
      },
      {
        path: 'dashboard',
        name: 'dashboard-view',
        component: () => import('@/views/Dashboard.vue'),
      },
      {
        path: 'sensors-data',
        name: 'sensors-data-view',
        component: () => import('@/views/sensors-data/SensorsDataView.vue'),
      },
      {
        path: 'statistics',
        name: 'statistics-view',
        component: () => import('@/views/statistics/StatisticsView.vue'),
      },
    ],
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
