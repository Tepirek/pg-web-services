import App from './App.vue';
import axios from 'axios';

import { createApp } from 'vue';
import { registerPlugins } from '@/plugins';

const app = createApp(App);

registerPlugins(app);

axios.defaults.headers.common['Content-Type'] = 'application/json';
axios.defaults.baseURL = import.meta.env.VITE_API_URL;
axios.defaults.timeout = 0;

app.mount('#app');
