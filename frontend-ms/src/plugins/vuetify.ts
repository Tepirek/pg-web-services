import '@mdi/font/css/materialdesignicons.css';
import 'vuetify/styles';

import { createVuetify } from 'vuetify';
import * as components from 'vuetify/components';
import * as labsComponents from 'vuetify/labs/components';

export default createVuetify({
  theme: {
    themes: {
      light: {
        colors: {
          primary: '#586f56',
          secondary: '#738d76',
        },
      },
    },
  },
  components: {
    ...components,
    ...labsComponents,
  },
});

