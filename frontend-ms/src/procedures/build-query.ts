export default (params: object = {}) => {
  const queryParams: string[] = [];
  Object.entries(params).forEach(([key, value]) => {
    queryParams.push(`${ key }=${ value }`);
  });
  return queryParams.length ? `?${ queryParams.join('&') }` : '';
};
