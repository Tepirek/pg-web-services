import { defineComponent } from 'vue';
import { DefaultPagination, Pagination } from '@/types/Pagination';

interface Data {
  pagination: Pagination | undefined
}

export default defineComponent({
  data(): Data {
    return {
      pagination: undefined,
    };
  },
  created() {
    this.pagination = DefaultPagination(this.$route.query);
  },
  methods: {
    updatePagination(pagination: Pagination) {
      this.pagination = DefaultPagination(pagination);
      this.$router.replace({ name: this.$route.name, query: { page: pagination.page, perPage: pagination.perPage } });
    },
  },
});
