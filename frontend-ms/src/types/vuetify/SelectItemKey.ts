export type SelectItemKey =
  boolean
  | string
  | (string | number)[]
  | ((item: Record<string, any>, fallback?: any) => any);
