import { SelectItemKey } from '@/types/vuetify/SelectItemKey';
import { DataTableCompareFunction } from '@/types/vuetify/DataTableCompareFunction';

export type DataTableHeader = {
  key: string;
  value?: SelectItemKey;
  title: string;
  colspan?: number;
  rowspan?: number;
  fixed?: boolean;
  align?: 'start' | 'end';
  width?: number;
  minWidth?: string;
  maxWidth?: string;
  sortable?: boolean;
  sort?: DataTableCompareFunction;
};
