export type DataTableCompareFunction<T = any> = (a: T, b: T) => number;
