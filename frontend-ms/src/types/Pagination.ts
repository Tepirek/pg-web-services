export type Pagination = {
  page: number;
  perPage: number;
  total: number;
  count: number;
};

export const DefaultPagination = (data: Partial<Pagination> = {}) => ({
  page: 1,
  perPage: 25,
  total: 1,
  count: 25,
  ...data,
});
