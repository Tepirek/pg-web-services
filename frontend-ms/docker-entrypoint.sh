#!/bin/bash

if [ ! -d node_modules ]; then
  npm ci && npm clear cache
fi

npm run dev
