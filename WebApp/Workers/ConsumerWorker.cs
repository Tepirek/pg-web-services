using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using System.Text;
using System.Text.Json;
using System.Xml;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using WebApp.DataLayer;
using WebApp.Models;
using WebApp.Services;
using MongoDB.Bson;
using MongoDB.Driver;

namespace WebApp.Workers;

public class ConsumerWorker : BackgroundService
{
    private readonly IConnection _rabbitMqConnection;
    private readonly MongoHelper _mongoHelper;

    public ConsumerWorker(RabbitMqConnectionService rabbitMqConnectionService, MongoHelper mongoHelper)
    {
        _rabbitMqConnection = rabbitMqConnectionService.Connection;
        _mongoHelper = mongoHelper;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        using (var channel = _rabbitMqConnection.CreateModel())
        {
            channel.QueueDeclare("sensors-data", true, false, false, null);
            channel.QueueDeclare("dashboard-data", false, false, false, null);

            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += async (sender, eventArgs) =>
            {
                var body = eventArgs.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);
                // Console.WriteLine($"Message received: {message}");
                DataModel? dataModel = JsonSerializer.Deserialize<DataModel>(message);

                if (dataModel != null)
                {
                    dataModel.SetUnitBasedOnType();
                    await _mongoHelper.CreateDocument<DataModel>("web-services", "sensors_data", dataModel);

                    var filter = Builders<DataModel>.Filter.Eq("Type", $"{dataModel.Type}");
                    var instanceFilter = Builders<DataModel>.Filter.Eq("Instance", $"{dataModel.Instance}");
                    filter = Builders<DataModel>.Filter.And(filter, instanceFilter);
                    var sort = Builders<DataModel>.Sort.Descending(d => d.Timestamp);
                    var documents =
                        await _mongoHelper.GetFilteredSortedLimitedDocuments<DataModel>("web-services", "sensors_data",
                            filter, sort);
                    var sum = documents.Sum(document => document.Value);
                    var average = sum / documents.Count;

                    DashboardDataModel dashboardDataModel = new DashboardDataModel
                    {
                        Type = dataModel.Type,
                        Instance = dataModel.Instance,
                        Timestamp = dataModel.Timestamp,
                        Value = dataModel.Value.ToString("0.00"),
                        Unit = dataModel.Unit,
                        Avg = average.ToString("0.00")
                    };

                    var dataToSend = JsonSerializer.Serialize(dashboardDataModel);
                    var dashboardBody = Encoding.UTF8.GetBytes(dataToSend);
                    channel.BasicPublish("", "dashboard-data", null, dashboardBody);
                    // Console.WriteLine($"Message to dashboard sent: {dataToSend}");
                }
            };
            channel.BasicConsume("sensors-data", true, consumer);

            while (!stoppingToken.IsCancellationRequested)
            {
                await Task.Delay(1000, stoppingToken);
            }

            channel.Close();
            consumer.Model = null;
        }
    }
}