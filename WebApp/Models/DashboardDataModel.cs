namespace WebApp.Models;

public class DashboardDataModel
{
    public string? Type { get; set; }
    public int Instance { get; set; }
    public int Timestamp { get; set; }
    public string Value { get; set; }
    public string? Unit { get; set;  }
    public string Avg { get; set; }
}