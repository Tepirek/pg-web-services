using System.Text.Json.Serialization;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace WebApp.Models;

public class DataModel
{

    [BsonId]
    [JsonIgnore]
    public ObjectId _id { get; set; }
    public string? Type { get; set; }
    public int Instance { get; set; }
    public int Timestamp { get; set; }
    public double Value { get; set; }
    public string? Unit { get; set;  }

    public void SetUnitBasedOnType()
    {
        if (this.Type == "temp")
        {
            this.Unit = "C";
        }
        else if (this.Type == "co2")
        {
            this.Unit = "ppm";
        }
        else if (this.Type == "pH")
        {
            this.Unit = "";
        }
        else if (this.Type == "light")
        {
            this.Unit = "lux";
        }
        else
        {
            this.Unit = "unknown";
        }
    }
}