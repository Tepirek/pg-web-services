using Microsoft.VisualBasic;

namespace WebApp.Models;

public class Filters
{
    public string? dateFrom { get; set; }
    public string? dateTo { get; set; }
    public string? type { get; set; }
    public string? instance { get; set; }
}