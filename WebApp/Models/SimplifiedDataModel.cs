namespace WebApp.Models;

public class SimplifiedDataModel
{
    public string? Type { get; set; }
    public string? Instance { get; set; }
    public string? Date { get; set; }
    public string? Value { get; set; }
    public string? Unit { get; set; }
}