using System.Text.Json.Serialization;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace WebApp.Models;

public class ResponseDataModel
{

    [BsonId]
    [JsonIgnore]
    public ObjectId _id { get; set; }
    public string? Type { get; set; }
    public int Instance { get; set; }
    public int Timestamp { get; set; }
    public string? Value { get; set; }
    public string? Unit { get; set;  }
}