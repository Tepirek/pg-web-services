using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WebApp.DataLayer
{
    public class MongoHelper : IMongoHelper
    {
        private readonly IConfiguration _configuration;
        public MongoHelper(IConfiguration config)
        {
            _configuration = config;
        }

        public IMongoDatabase GetMongoDbInstance(string dbName)
        {
            var connString = _configuration.GetConnectionString("MongoDB");
            var client = new MongoClient(connString);
            var db = client.GetDatabase(dbName);
            return db;
        }

        public IMongoCollection<T> GetCollection<T>(string dbName, string collectionName)
        {
            return GetMongoDbInstance(dbName).GetCollection<T>(collectionName);
        }

        public async Task<List<T>> GetAllDocuments<T>(string dbName, string collectionName, int page = 1, int perPage = 10)
        {
            int skip = (page - 1) * perPage;
            return await GetCollection<T>(dbName, collectionName)
                .Find(x => true)
                .Skip(skip)
                .Limit(perPage)
                .ToListAsync();
        }
        
        public async Task<List<T>> GetFilteredDocuments<T>(string dbName, string collectionName, FilterDefinition<T> filter, int page = 1, int perPage = 10)
        {
            int skip = (page - 1) * perPage;
            return await GetCollection<T>(dbName, collectionName)
                .Find(filter)
                .Skip(skip)
                .Limit(perPage)
                .ToListAsync();
        }
        public async Task<List<T>> GetFilteredSortedPagedDocuments<T>(string dbName, string collectionName, FilterDefinition<T> filter, SortDefinition<T> sort, int page = 1, int perPage = 10)
        {
            int skip = (page - 1) * perPage;
            return await GetCollection<T>(dbName, collectionName)
                .Find(filter)
                .Sort(sort)
                .Skip(skip)
                .Limit(perPage)
                .ToListAsync();
        }
        public async Task<List<T>> GetFilteredSortedDocuments<T>(string dbName, string collectionName, FilterDefinition<T> filter, SortDefinition<T> sort)
        {
            return await GetCollection<T>(dbName, collectionName)
                .Find(filter)
                .Sort(sort)
                .ToListAsync();
        }
        
        public async Task<List<T>> GetFilteredSortedLimitedDocuments<T>(string dbName, string collectionName, FilterDefinition<T> filter, SortDefinition<T> sort, int limit = 100)
        {
            var collection = GetCollection<T>(dbName, collectionName);
            var filteredDocuments = await collection
                .Find(filter)
                .Sort(sort)
                .Limit(limit)
                .ToListAsync();
            return filteredDocuments;
        }

        public async Task UpdateDocument<T>(string dbName, string collectionName, FilterDefinition<T> filter, UpdateDefinition<T> document)
        {
            await GetCollection<T>(dbName, collectionName).UpdateOneAsync(filter, document);
        }

        public async Task CreateDocument<T>(string dbName, string collectionName, T document)
        {
            await GetCollection<T>(dbName, collectionName).InsertOneAsync(document);
        }

        public async Task DeleteDocument<T>(string dbName, string collectionName, FilterDefinition<T> filter)
        {
            await GetCollection<T>(dbName, collectionName).DeleteOneAsync(filter);
        }
    }
}