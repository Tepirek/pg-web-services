using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WebApp.DataLayer
{
  public interface IMongoHelper
  {
    Task<List<T>> GetAllDocuments<T>(string dbName, string collectionName, int page, int perPage);
    Task<List<T>> GetFilteredDocuments<T>(string dbName, string collectionName, FilterDefinition<T> filter, int page, int perPage);
    Task UpdateDocument<T>(string dbName, string collectionName, FilterDefinition<T> filter, UpdateDefinition<T> document);
    Task CreateDocument<T>(string dbName, string collectionName, T document);
    Task DeleteDocument<T>(string dbName, string collectionName, FilterDefinition<T> filter);
  }
}