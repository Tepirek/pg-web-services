FROM mcr.microsoft.com/dotnet/sdk:7.0 AS build

WORKDIR /app

COPY . .

RUN dotnet add package RabbitMQ.Client --version 6.6.0

RUN dotnet add package MongoDB.Driver --version 2.22.0

RUN dotnet add package Newtonsoft.Json --version 13.0.3

RUN dotnet restore

ENV DOTNET_USE_POLLING_FILE_WATCHER 1

ENTRYPOINT [ "dotnet", "watch", "run", "--urls", "http://0.0.0.0:5042" ]
