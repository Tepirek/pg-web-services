using WebApp.DataLayer;
using WebApp.Services;
using WebApp.Workers;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();

builder.Services.AddSingleton<RabbitMqConnectionService>();

builder.Services.AddSingleton<MongoHelper>();

builder.Services.AddHostedService<ConsumerWorker>();

var app = builder.Build();

// Configure the HTTP request pipeline.

app.UseHttpsRedirection();

app.UseCors(options => options.AllowAnyOrigin());

app.UseAuthorization();

app.MapControllers();

app.Run();