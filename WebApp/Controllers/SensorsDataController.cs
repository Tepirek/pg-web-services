using System.Net;
using System.Text.Json;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using MongoDB.Bson;
using MongoDB.Bson.IO;
using MongoDB.Driver;
using WebApp.DataLayer;
using WebApp.Formatters;
using WebApp.Models;
using Newtonsoft.Json;

namespace WebApp.Controllers;

// http://localhost:8888/mongo?
// page=1&
// perPage=100&
// filters={
//  dateFrom:2023-10-18,
//  dateTo:2023-10-31,
//  type:temp,
//  instance:2}&
// sortBy=date&
// order=asc

// http://localhost:8888/mongo?page=1&perPage=100&filters={dateFrom:2023-10-18,dateTo:2023-10-31,type:temp,instance:2}&sortBy=date&order=asc

[ApiController]
[Route("[controller]")]
public class SensorsDataController : ControllerBase
{
    private readonly ILogger<SensorsDataController> _logger;
    private readonly MongoHelper _mongoHelper;

    public SensorsDataController(ILogger<SensorsDataController> logger, MongoHelper mongo)
    {
        _logger = logger;
        _mongoHelper = mongo;
    }


    [HttpGet]
    public async Task<OkObjectResult> Get(
        [FromQuery] int page = 1,
        [FromQuery] int perPage = 10,
        [FromQuery] string filters = "{}",
        [FromQuery] string sortBy = "",
        [FromQuery] string order = "asc",
        [FromQuery] string list = "false"
    )
    {
        long totalItems = _mongoHelper.GetCollection<DataModel>("web-services", "sensors_data")
            .CountDocuments(new BsonDocument());
        var totalPages = (int)Math.Ceiling((double)totalItems / perPage);

        var dictionary = System.Text.Json.JsonSerializer.Deserialize<Filters>(filters);

        var filter = Builders<DataModel>.Filter.Empty;

        var sort = Builders<DataModel>.Sort.Ascending(d => d._id);

        if (!string.IsNullOrEmpty(dictionary?.dateFrom))
        {
            var startDate = DateTime.Parse(dictionary.dateFrom);
            var startTimestamp =
                (long)(startDate.ToUniversalTime() - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds;
            var startDateFilter = Builders<DataModel>.Filter.Gte("Timestamp", startTimestamp);
            filter = Builders<DataModel>.Filter.And(filter, startDateFilter);
        }

        if (!string.IsNullOrEmpty(dictionary?.dateTo))
        {
            var endDate = DateTime.Parse(dictionary.dateTo);
            var endTimestamp = (long)(endDate.ToUniversalTime() - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc))
                .TotalSeconds;
            var endDateFilter = Builders<DataModel>.Filter.Lt("Timestamp", endTimestamp);
            filter = Builders<DataModel>.Filter.And(filter, endDateFilter);
        }

        if (!string.IsNullOrEmpty(dictionary?.type))
        {
            var typeFilter = Builders<DataModel>.Filter.Eq("Type", dictionary.type);
            filter = Builders<DataModel>.Filter.And(filter, typeFilter);
        }

        if (!string.IsNullOrEmpty(dictionary?.instance))
        {
            var instanceFilter = Builders<DataModel>.Filter.Eq("Instance", int.Parse(dictionary.instance));
            filter = Builders<DataModel>.Filter.And(filter, instanceFilter);
        }

        if (sortBy != "")
        {
            if (order == "asc")
            {
                sort = Builders<DataModel>.Sort.Ascending(sortBy.FirstCharToUpper());
            }
            else
            {
                sort = Builders<DataModel>.Sort.Descending(sortBy.FirstCharToUpper());
            }
        }

        List<DataModel> items = null;
        object? response = null;
        if (list == "true")
        {
            items = await _mongoHelper.GetFilteredSortedDocuments<DataModel>(
                "web-services",
                "sensors_data",
                filter,
                sort
            );
            
            List<ResponseDataModel> responseItems = items.Select(data => new ResponseDataModel
            {
                _id = data._id,
                Instance = data.Instance,
                Type = data.Type,
                Value = data.Value.ToString("0.00"),
                Unit = data.Unit,
                Timestamp = data.Timestamp
            }).ToList();
            
            response =  new
            {
                items = responseItems,
                count = totalItems
            };
        }
        else
        {
            items = await _mongoHelper.GetFilteredSortedPagedDocuments<DataModel>(
                "web-services",
                "sensors_data",
                filter,
                sort,
                page,
                perPage
            );
            
            List<ResponseDataModel> responseItems = items.Select(data => new ResponseDataModel
            {
                _id = data._id,
                Instance = data.Instance,
                Type = data.Type,
                Value = data.Value.ToString("0.00"),
                Unit = data.Unit,
                Timestamp = data.Timestamp
            }).ToList();
            
            response = new
            {
                items = responseItems,
                pagination = new
                {
                    page,
                    perPage,
                    total = totalPages,
                    count = totalItems
                }
            };
        }

        return Ok(response);
    }

    [HttpGet("download")]
    public async Task<FileResult> Download(
        [FromQuery] int page = 1,
        [FromQuery] int perPage = 10,
        [FromQuery] string filters = "{}",
        [FromQuery] string sortBy = "",
        [FromQuery] string order = "asc"
    )
    {
        long totalItems = _mongoHelper.GetCollection<DataModel>("web-services", "sensors_data")
            .CountDocuments(new BsonDocument());
        var totalPages = (int)Math.Ceiling((double)totalItems / perPage);

        var dictionary = System.Text.Json.JsonSerializer.Deserialize<Filters>(filters);

        var filter = Builders<DataModel>.Filter.Empty;

        var sort = Builders<DataModel>.Sort.Ascending(d => d._id);

        if (!string.IsNullOrEmpty(dictionary?.dateFrom))
        {
            var startDate = DateTime.Parse(dictionary.dateFrom);
            var startTimestamp =
                (long)(startDate.ToUniversalTime() - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds;
            var startDateFilter = Builders<DataModel>.Filter.Gte("Timestamp", startTimestamp);
            filter = Builders<DataModel>.Filter.And(filter, startDateFilter);
        }

        if (!string.IsNullOrEmpty(dictionary?.dateTo))
        {
            var endDate = DateTime.Parse(dictionary.dateTo);
            var endTimestamp = (long)(endDate.ToUniversalTime() - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc))
                .TotalSeconds;
            var endDateFilter = Builders<DataModel>.Filter.Lt("Timestamp", endTimestamp);
            filter = Builders<DataModel>.Filter.And(filter, endDateFilter);
        }

        if (!string.IsNullOrEmpty(dictionary?.type))
        {
            var typeFilter = Builders<DataModel>.Filter.Eq("Type", dictionary.type);
            filter = Builders<DataModel>.Filter.And(filter, typeFilter);
        }

        if (!string.IsNullOrEmpty(dictionary?.instance))
        {
            var instanceFilter = Builders<DataModel>.Filter.Eq("Instance", int.Parse(dictionary.instance));
            filter = Builders<DataModel>.Filter.And(filter, instanceFilter);
        }

        if (sortBy != "")
        {
            if (order == "asc")
            {
                sort = Builders<DataModel>.Sort.Ascending(sortBy.FirstCharToUpper());
            }
            else
            {
                sort = Builders<DataModel>.Sort.Descending(sortBy.FirstCharToUpper());
            }
        }

        var items = await _mongoHelper.GetFilteredSortedPagedDocuments<DataModel>(
            "web-services",
            "sensors_data",
            filter,
            sort, page,
            perPage
        );
        
        if (Request.Headers.ContainsKey("Accept"))
        {
            string acceptHeader = Request.Headers["Accept"];
            Dictionary<string, string> types = new Dictionary<string, string>();
            types["temp"] = "Temperatura powietrza";
            types["co2"] = "Poziom CO2";
            types["light"] = "Poziom oświetlenia";
            types["pH"] = "pH gleby";
            if (acceptHeader == "application/json")
            {
                List<SimplifiedDataModel> simplifiedItems = items.Select(data => new SimplifiedDataModel
                {
                    Instance = data.Instance.ToString(),
                    Type = types[data.Type],
                    Value = Math.Round(data.Value, 2).ToString("0.00"),
                    Unit = data.Unit,
                    Date = DateTimeOffset.FromUnixTimeSeconds(data.Timestamp).DateTime.ToString("yyyy-MM-dd HH:mm:ss")
                }).ToList(); 
                string json = Newtonsoft.Json.JsonConvert.SerializeObject(simplifiedItems);
                var stream = new MemoryStream();
                var writer = new StreamWriter(stream);
                writer.Write(json);
                writer.Flush();
                stream.Position = 0;

                return File(stream, "application/json", "items.json");
            }
            else if (acceptHeader == "text/csv")
            {
                string csv = "Instance,Type,Value,Unit,Date\n";
                foreach (var item in items)
                {
                    var type = types[item.Type];
                    var roundedValue = Math.Round(item.Value, 2).ToString("0.00");
                    DateTime dateTime = DateTimeOffset.FromUnixTimeSeconds(item.Timestamp).DateTime;
                    string dateString = dateTime.ToString("yyyy-MM-dd HH:mm:ss");
                    csv += $"{item.Instance},{type},{roundedValue},{item.Unit},{dateString}\n";
                }
                var stream = new MemoryStream();
                var writer = new StreamWriter(stream);
                writer.Write(csv);
                writer.Flush();
                stream.Position = 0;
                
                return File(stream, "text/csv", "items.csv");
            }
        }
        return File(new byte[0], "text/plain", "items.txt");
    }
}