using RabbitMQ.Client;

namespace WebApp.Services;

public class RabbitMqConnectionService
{
    public IConnection Connection { get; }
    
    public RabbitMqConnectionService()
    {
        // Initialize and configure your RabbitMQ connection here
        var factory = new ConnectionFactory
        {
            HostName = "rabbit", // RabbitMQ server hostname
            UserName = "root",
            Password = "root",
            // Add any other configuration options here
        };

        Connection = factory.CreateConnection();
    }
}