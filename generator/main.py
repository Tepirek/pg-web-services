import argparse
import random
import sys
import time
from dataclasses import dataclass
from datetime import datetime
from typing import Tuple

import docker
from faker import Faker
from faker.providers import date_time

from rabbit import *

faker = Faker()
faker.add_provider(date_time)


def get_container_name(id) -> str:
    client = docker.from_env()
    container = client.containers.get(id)
    return container.name


@dataclass
class Sensor:
    type: str
    value_range: Tuple[float, float]
    measurement_unit: str


temperature_sensor = Sensor("temp", (20, 28), "°C")
co2_sensor = Sensor("co2", (300, 800), "ppm")
pH_sensor = Sensor("pH", (6.0, 7.0), "")
light_sensor = Sensor("light", (400, 600), "µmol/s/m²")


def generate_value(value_range: Tuple[float, float]) -> float:
    mu = sum(value_range) / len(value_range)
    sigma = (value_range[1] - value_range[0]) / 7
    value = random.gauss(mu, sigma)
    value = max(value_range[0], min(value_range[1], value))
    return value


# Returns list of tuples (value, type, instance)
# def generate_sensors_data(sensor: Sensor) -> [(float, str, int)]:
#     values = []
#     for i in range(4):
#         values.append((generate_value(sensor.value_range), sensor.type, i + 1))
#     return values

def generate_data(env_type: str, replica_id: int, channel: BlockingChannel, queue_name: str):
    timestamp = int(datetime.timestamp(faker.date_time()))

    value = 0.0
    match env_type:
        case "temp":
            value = generate_value(temperature_sensor.value_range)
        case "co2":
            value = generate_value(co2_sensor.value_range)
        case "pH":
            value = generate_value(pH_sensor.value_range)
        case "light":
            value = generate_value(light_sensor.value_range)
        case _:
            print("Unknown type")
            sys.exit()

    msg = create_rabbit_message(env_type, replica_id, timestamp, value)
    send_to_rabbitmq(msg, channel, queue_name)


def main(arg_type, arg_value, env_type, replica_id):
    sleep = random.uniform(5, 10)
    connection, channel, queue_name = open_rabbit_connection()

    if arg_type is None and arg_value is None:
        while True:
            generate_data(env_type, replica_id, channel, queue_name)
            time.sleep(sleep)
    elif arg_type is not None and arg_value is not None:
        timestamp = int(time.time())
        msg = create_rabbit_message(arg_type, replica_id, timestamp, arg_value)
        send_to_rabbitmq(msg, channel, queue_name)

    close_rabbit_connection(connection)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Process sensor data")
    parser.add_argument("--type", type=str, help="Sensor type", required=False)
    parser.add_argument("--value", type=float, help="Value for the sensor", required=False)

    container_name = get_container_name(os.environ.get("HOSTNAME"))
    try:
        replica_id = int(container_name.split("-")[-1])
        args = parser.parse_args()
    except ValueError as e:
        parser.add_argument("--instance", type=int, help="Sensor instance", required=False)
        args = parser.parse_args()
        replica_id = args.instance
    print(f"Replica ID: {replica_id}")

    if "TYPE" not in os.environ:
        sys.exit()
    else:
        try:
            type = str(os.environ.get("TYPE"))
        except ValueError:
            print(f"Environment variable TYPE cannot be converted to str")
            sys.exit()

    main(args.type, args.value, type, replica_id)
