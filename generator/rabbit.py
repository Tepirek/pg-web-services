import json
import os

import pika
from pika.adapters.blocking_connection import BlockingChannel

def create_rabbit_message(sensor_type: str, instance: int, timestamp: int, value: float) -> str:
    msg_dict = {}
    msg_dict.update({"Type": sensor_type})
    msg_dict.update({"Instance": instance})
    msg_dict.update({"Timestamp": timestamp})
    msg_dict.update({"Value": value})
    return json.dumps(msg_dict)

def open_rabbit_connection() -> (pika.BlockingConnection, BlockingChannel, str):
    credentials = pika.PlainCredentials(os.environ.get('RABBITMQ_USERNAME'), os.environ.get('RABBITMQ_PASSWORD'))
    parameters = pika.ConnectionParameters(
        host=os.environ.get('RABBITMQ_HOST'),
        port=int(os.environ.get('RABBITMQ_PORT')),
        credentials=credentials
    )
    connection = pika.BlockingConnection(parameters=parameters)
    channel = connection.channel()
    queue_name = os.environ.get('QUEUE_NAME')
    channel.queue_declare(queue=queue_name, durable=True)
    return connection, channel, queue_name

def close_rabbit_connection(connection: pika.BlockingConnection):
    connection.close()

def send_to_rabbitmq(message, channel, queue_name):
    channel.basic_publish(
        exchange='',
        routing_key=queue_name,
        body=message,
        properties=pika.BasicProperties(delivery_mode=pika.DeliveryMode.Persistent)
    )
    print(f'Sending message: {message}')
