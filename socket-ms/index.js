import express from 'express';
import {createServer} from 'node:http';
import {Server} from 'socket.io';
import amqp from "amqplib";

const config = {...process.env};

const app = express();
const server = createServer(app);
const io = new Server(server, {
    cors: {
        origin: process.env.ALLOWED_ORIGIN
    }
});

io.sockets.on('connection', (socket) => {
    console.log(`Client [${socket.id}] connected!`)
    socket.on('disconnect', () => {
        console.log(`Client [${socket.id}] disconnected!`);
    });
});

server.listen(3000, () => {
    console.log('Server running at http://localhost:3000');
});

(async () => {
    try {
        const url = `amqp://${config.RABBITMQ_USERNAME}:${config.RABBITMQ_PASSWORD}@${config.RABBITMQ_HOST}`
        const connection = await amqp.connect(url);
        const channel = await connection.createChannel();

        process.once("SIGINT", async () => {
            await channel.close();
            await connection.close();
        });

        await channel.assertQueue(config.QUEUE_NAME, {durable: false});

        await channel.consume(
            config.QUEUE_NAME,
            message => {
                io.sockets.emit('dashboard', JSON.parse(message.content.toString()));
                console.log(`Emit new message to all clients: ${message.content.toString()}`)
            },
            {
                noAck: true
            }
        );
    } catch (error) {
        console.warn(error);
    }
})();